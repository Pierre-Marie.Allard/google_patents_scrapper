import time 
import selenium
from selenium import webdriver
from selenium.webdriver import Chrome
from pprint import pprint
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
import pandas as pd
import numpy as np



# from webdriver_manager.chrome import ChromeDriverManager

# driver = webdriver.Chrome(ChromeDriverManager().install())

def check_page_query_loaded():
    try:
        WebDriverWait(driver, 10).until(
            ec.presence_of_element_located(
                (
                    By.XPATH,
                    '//*[@id="resultsContainer"]/section/search-result-item[1]/article',
                )
            )
        )
    except:
        pass
    # except NoSuchElementException:
    #     print('Error loading searching results page')
    #     pass
    # continue
    # sys.exit()


def check_page_result_loaded():
    try:
        WebDriverWait(driver, 10).until(
            ec.presence_of_element_located(
                (By.XPATH, '//*[@id="mainContainer"]/result-container')
            )
        )
    except:
        pass
    # except NoSuchElementException:
    #     print('Error loading searching results page')
    #     pass
    # continue
    # sys.exit()


### select the chromedriver path

DRIVER_PATH = "./resources/chromedriver"



# if your want to run in headless mode (without GUI)

options = Options()
options.headless = True
options.add_argument("--window-size=1920,1200")


## generating the search link

# Legacy inputs
IK = "PFEUJUXMHXWDQS-MHSWQEBQSA-N"
biosource = "Eurycoma longifolia"
ref = "none"
# IK = 'XSYCDVWYEVUDKQ-UHFFFAOYSA-N'
# biosource = 'Chimonanthus praecox'



# loading data


df = pd.read_csv("../curatedTable5000_shuffled_headed.tsv", sep="\t")
#df = pd.read_csv("../sanitized_table_sub1000.tsv", sep="\t")

#df = df[df.database.str.contains("npa_1")]

df.info()
df.head()


#using this line we can deduplicate according to two columns


# df = df.drop_duplicates(subset=['inchikeySanitized', 'organismSanitized', 'curatedJournal'], keep=False)

# df.dropna(subset=['inchikeySanitized', 'organismSanitized'])

# df[["chemo_inchikey", "bio_lowertaxon"]].dropna().sample(20)

# df["bio_lowertaxon" == 'Selaginella willdenowii']



# df_sub = df[["chemo_inchikey", "bio_lowertaxon"]].dropna().sample(50)

# chemo_inchikey_list = df_sub["chemo_inchikey"].to_list()
# bio_lowertaxon_list = df_sub["bio_lowertaxon"].to_list()

##

#df[["inchikeySanitized", "organism_lowertaxon", 'curatedTitle']].dropna().sample(200)

# df_sub = df[["inchikeySanitized", "organismSanitized", 'curatedTitle']].dropna().sample(400)

df_sub = df[["inchikeySanitized", "organismSanitized", 'curatedTitle']].dropna(subset=['inchikeySanitized', 'organismSanitized'])

df_sub = df_sub.sample(20)

chemo_inchikey_list = df_sub["inchikeySanitized"].to_list()
bio_lowertaxon_list = df_sub["organismSanitized"].to_list()
original_ref_list = df_sub["curatedTitle"].to_list()


len(chemo_inchikey_list)

chemo_inchikey_list.append(IK)
bio_lowertaxon_list.append(biosource)
original_ref_list.append(ref)

chemo_inchikey_list
bio_lowertaxon_list
original_ref_list




# timer is started
start_time = time.time()


ref_table = pd.DataFrame(
    columns=[
        "IK",
        "bio_lowertaxon",
        "original_ref",
        "query_link",
        "top1_link",
        "ref",
        "extracted_title",
        "extracted_journal",
        "extracted_year",
        "extracted_firstauthor_year",
    ]
)


for IK, biosource, ref in zip(chemo_inchikey_list, bio_lowertaxon_list, original_ref_list):
    print(IK)
    print(biosource)
    print(ref)

    biosource_plus = biosource.replace(" ", "+")
    #print(biosource_plus)
    # here for a loose search in the title
    # query_link = str(
    #     "https://patents.google.com/?q="
    #     + IK
    #     + "&q=TI%3d"
    #     + biosource_plus
    #     + "&patents=false&scholar"
    # )
    # # here for a strict search in the title (note the "") and the necessity to escape them \"
    # query_link = str(
    #     "https://patents.google.com/?q="
    #     + IK
    #     + "&q=TI%3d\""
    #     + biosource_plus
    #     + "\"&patents=false&scholar"
    # )
    # here for a strict  search 
    # query_link = str(
    #     "https://patents.google.com/?q="
    #     + IK
    #     + "&q="
    #     + biosource_plus
    #     + "&patents=false&scholar"
    # )
    # here for a strict and older results first search in the title (note the "") and the necessity to escape them \"
    query_link = str(
        "https://patents.google.com/?q="
        + IK
        + "&q=TI%3d\""
        + biosource_plus
        + "\"&patents=false&scholar&sort=old"
    )
    query_link

    

    driver = webdriver.Chrome(options=options, executable_path=DRIVER_PATH)
    # driver.get("https://patents.google.com/?q=PFEUJUXMHXWDQS-MHSWQEBQSA-N&patents=false&scholar&oq=PFEUJUXMHXWDQS-MHSWQEBQSA-N")
    driver.get(query_link)
    #print(driver.page_source)
    print(query_link)
    driver_current_url = driver.current_url
    #driver.save_screenshot("screenshot.png")

    # Now let's work on the first element

    

    try:
        WebDriverWait(driver, 1.5).until(
            ec.presence_of_element_located(
                (
                    By.XPATH,
                    '//*[@id="resultsContainer"]/section/search-result-item[1]/article',
                )
            )
        )
        
        # we check if the first result publication date is different than 0
        
        result_n = 1
        pub_date_value = 'Published 0'
        print(result_n)

        while pub_date_value is 'Published 0':

            print(pub_date_value)

            pub_date = driver.find_element_by_xpath(
                str('//*[@id="resultsContainer"]/section/search-result-item['
                + str(result_n)
                + ']/article/div/div/div[1]/h4[2]')
                )
            pub_date_value = pub_date.text
            print(pub_date_value)
            

            if pub_date_value is not 'Published 0':

                #print(result_n)
        
                result_one = driver.find_element_by_xpath(
                    str('//*[@id="resultsContainer"]/section/search-result-item['
                        + str(result_n)
                        + ']/article/state-modifier')
                        )

                top_one_id = result_one.get_attribute("data-result")
                top_one_id

                # top one link to follow  is

                top1_link = str(
                    'https://patents.google.com/'
                    + top_one_id
                    #+ '?q=PFEUJUXMHXWDQS-MHSWQEBQSA-N&patents=false&scholar&oq=PFEUJUXMHXWDQS-MHSWQEBQSA-N'
                    )
                top1_link
                # we now acces this page

                driver.get(top1_link)

                # we retrieve the associated DOI
                try:
                    WebDriverWait(driver, 1.5).until(
                        ec.presence_of_element_located(
                            (By.XPATH, '//*[@id="mainContainer"]/result-container')
                        )
                    )

                    doi = driver.find_element_by_xpath('//*[@id="abstract"]/div[2]/a[1]')
                    title = driver.find_element_by_xpath('//*[@id="title"]')
                    journal = driver.find_element_by_xpath(
                        '//*[@id="wrapper"]/div[1]/div[2]/section/dl[2]/dd[2]'
                    )
                    year = driver.find_element_by_xpath(
                        '//*[@id="wrapper"]/div[1]/div[2]/section/dl[2]/dd[1]/state-modifier'
                    )
                    first_author_year = driver.find_element_by_xpath(
                        '//*[@id="wrapper"]/div[1]/div[2]/section/header/h2'
                    )

                    doi_link = doi.get_attribute("href")
                    title_extracted = title.text
                    journal_extracted = journal.text
                    year_extracted = year.text
                    first_author_year_extracted = first_author_year.text

                    print(doi_link)
                    print(title_extracted)
                    print(journal_extracted)
                    print(year_extracted)
                    print(first_author_year_extracted)

                    driver.quit()

                    ref_table.loc[len(ref_table)] = [
                        IK,
                        biosource,
                        ref,
                        driver_current_url,
                        top1_link,
                        doi_link,
                        title_extracted,
                        journal_extracted,
                        year_extracted,
                        first_author_year_extracted
                    ]
                except:
                    ref_table.loc[len(ref_table)] = [
                        IK,
                        biosource,
                        ref,
                        driver_current_url,
                        top1_link,
                        "NaN",
                        "NaN",
                        "NaN",
                        "NaN",
                        "NaN"
                    ]
                    pass

            elif pub_date_value is 'Published 0':
                result_n += 1
                print("Results_n is now " + result_n)
    except:
        ref_table.loc[len(ref_table)] = [IK, biosource, ref, driver_current_url, "NaN", "NaN", "NaN", "NaN", "NaN", "NaN"]
        pass


ref_table.to_csv("curated_table_allsub_now.csv")


#timer is stopped
print(" Above command executed in --- %s seconds ---" %
      (time.time() - start_time))

