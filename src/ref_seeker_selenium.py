from selenium import webdriver
from selenium.webdriver import Chrome
import pandas as pd
from pprint import pprint
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By



def check_page_query_loaded():
    try:
        WebDriverWait(driver, 10).until(
            ec.presence_of_element_located((
                By.XPATH, '//*[@id="resultsContainer"]/section/search-result-item[1]/article')))
    except:
        print('Error loading searching results page')
        sys.exit()



def check_page_result_loaded():
    try:
        WebDriverWait(driver, 10).until(
            ec.presence_of_element_located((
                By.XPATH, '//*[@id="mainContainer"]/result-container')))
    except:
        print('Error loading searching results page')
        sys.exit()





### select the chromedriver path 

DRIVER_PATH = './resources/chromedriver'


# if your want to run in headless mode (without GUI)

options = Options()
options.headless = True
options.add_argument("--window-size=1920,1200")


## generating the search link 

# Legacy inputs 
IK = 'PFEUJUXMHXWDQS-MHSWQEBQSA-N'
biosource = 'Eurycoma longifolia'
# IK = 'XSYCDVWYEVUDKQ-UHFFFAOYSA-N'
# biosource = 'Chimonanthus praecox'
	

biosource.replace(" ", "+")


query_link = str('https://patents.google.com/?q=' + IK + '+AND+' + biosource + '&patents=false&scholar')
query_link

driver = webdriver.Chrome(options=options, executable_path=DRIVER_PATH)
#driver.get("https://patents.google.com/?q=PFEUJUXMHXWDQS-MHSWQEBQSA-N&patents=false&scholar&oq=PFEUJUXMHXWDQS-MHSWQEBQSA-N")
driver.get(query_link)
#print(driver.page_source)
print(driver.title)
print(driver.current_url)
driver.save_screenshot('screenshot.png')

# Now let's work on the first element




check_page_query_loaded()
result_one = driver.find_element_by_xpath('/html/body/search-app/search-results/search-ui/div/div/div[2]/div/div/div[1]/section/search-result-item[1]/article/state-modifier')
top_one_id = result_one.get_attribute('data-result')
top_one_id

# top one link to follow  is 

top1_link = str('https://patents.google.com/' + top_one_id + '?q=PFEUJUXMHXWDQS-MHSWQEBQSA-N&patents=false&scholar&oq=PFEUJUXMHXWDQS-MHSWQEBQSA-N')
top1_link


#we now acces this page 

driver.get(top1_link)

# we retrieve the associated DOI
check_page_result_loaded()

doi = driver.find_element_by_xpath('//*[@id="abstract"]/div[2]/a[1]')

doi_link = doi.get_attribute('href')

print(doi_link)

driver.quit()




