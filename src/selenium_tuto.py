
### following https://towardsdatascience.com/web-scraping-using-selenium-python-8a60f4cf40ab


from selenium.webdriver import Chrome
import pandas as pd

webdriver = './resources/chromedriver'

driver = Chrome(webdriver)

driver.get('https://forums.edmunds.com/discussion/2864/general/x/entry-level-luxury-performance-sedans/')

#after inspection of the author name here is the collected xpath

# xpath = //*[@id="Discussion_2864"]/div/div[1]/div[1]/span[1]/a[2]

# to find element in the xpath 

userid_element = driver.find_elements_by_xpath('//*[@id="Discussion_2864"]/div/div[1]/div[1]/span[1]/a[2]')[0]
userid = userid_element.text

userid_element

# now we check for the date

# //*[@id="Discussion_2864"]/div/div[1]/div[2]/span[1]/a/time

user_date = driver.find_elements_by_xpath('//*[@id="Discussion_2864"]/div/div[1]/div[2]/span[1]/a/time')[0]
date = user_date.get_attribute('title')

date

# Now we extract the comment 
# //*[@id="Discussion_2864"]/div/div[2]/div/div[1]

user_message = driver.find_elements_by_xpath('//*[@id="Discussion_2864"]/div/div[2]/div/div[1]')[0]
comment = user_message.text

comment


# we check all comments ids on a particular page

ids = driver.find_elements_by_xpath("//*[contains(@id,'Comment_')]")
comment_ids = []
for i in ids:
    comment_ids.append(i.get_attribute('id'))

comment_ids


driver.get('https://forums.edmunds.com/discussion/2864/general/x/entry-level-luxury-performance-sedans/')

comments = pd.DataFrame(columns = ['Date','user_id','comments']) 
ids = driver.find_elements_by_xpath("//*[contains(@id,'Comment_')]")
comment_ids = []
for i in ids:
    comment_ids.append(i.get_attribute('id'))

for x in comment_ids:
    #Extract dates from for each user on a page
    user_date = driver.find_elements_by_xpath('//*[@id="' + x +'"]/div/div[2]/div[2]/span[1]/a/time')[0]
    date = user_date.get_attribute('title')

    #Extract user ids from each user on a page
    userid_element = driver.find_elements_by_xpath('//*[@id="' + x +'"]/div/div[2]/div[1]/span[1]/a[2]')[0]
    userid = userid_element.text

    #Extract Message for each user on a page
    user_message = driver.find_elements_by_xpath('//*[@id="' + x +'"]/div/div[3]/div/div[1]')[0]
    comment = user_message.text
                                   
    #Adding date, userid and comment for each user in a dataframe    
    comments.loc[len(comments)] = [date,userid,comment]


comments