from selenium import webdriver
from selenium.webdriver import Chrome
import pandas as pd
from pprint import pprint
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By



## generating the search link 

# Legacy inputs 
IK = 'PFEUJUXMHXWDQS-MHSWQEBQSA-N'
biosource = 'Eurycoma longifolia'
# IK = 'XSYCDVWYEVUDKQ-UHFFFAOYSA-N'
# biosource = 'Chimonanthus praecox'

options = Options()
options.add_argument("--headless")
options.add_argument("--incognito")
driverpath = './resources/chromedriver'
driver = webdriver.Chrome(options=options, executable_path=driverpath)
query_link = str('https://patents.google.com/?q=' + IK + '+AND+' + biosource + '&patents=false&scholar')
number_of_results = None
links = []
titles = []



def check_page_query_loaded():
    try:
        WebDriverWait(driver, 10).until(
            ec.presence_of_element_located((
                By.XPATH, '//*[@id="resultsContainer"]/section/search-result-item[1]/article')))
    except:
        print('Error loading searching results page')
        sys.exit()


def check_page_result_loaded():
    try:
        WebDriverWait(driver, 10).until(
            ec.presence_of_element_located((
                By.XPATH, '//*[@id="mainContainer"]/result-container')))
    except:
        print('Error loading searching results page')
        sys.exit()



def querier(IK, biosource):

    driver = webdriver.Chrome(options=options, executable_path=driverpath)

    biosource = biosource.replace(" ", "+")
    driver.get(query_link)
    print(driver.title)
    print(driver.current_url)
    driver.save_screenshot('screenshot.png')      
    # Now let's work on the first element
    check_page_query_loaded()
    result_one = driver.find_element_by_xpath('//*[@id="resultsContainer"]/section/search-result-item[1]/article/state-modifier')
    top_one_id = result_one.get_attribute('data-result')
    print(top_one_id)
    # top one link to follow  is 
    top1_link = str('https://patents.google.com/' + top_one_id + '?q=PFEUJUXMHXWDQS-MHSWQEBQSA-N&patents=false&scholar&oq=PFEUJUXMHXWDQS-MHSWQEBQSA-N')
    print(top1_link)
    #we now acces this page 
    driver.get(top1_link)
    # we retrieve the associated DOI
    check_page_result_loaded()
    doi = driver.find_element_by_xpath('//*[@id="abstract"]/div[2]/a[1]')
    doi_link = doi.get_attribute('href')
    print(doi_link)
    return doi_link
    driver.quit()


querier(IK, biosource)

driver.save_screenshot('screenshot.png') 
result_one = driver.find_element_by_xpath('/html/body/search-app/search-results/search-ui/div/div/div[2]/div/div/div[1]/section/search-result-item[1]/article/state-modifier')