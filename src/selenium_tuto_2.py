
### following https://www.scrapingbee.com/blog/selenium-python/

from selenium.webdriver import Chrome
import pandas as pd

DRIVER_PATH = './resources/chromedriver'

driver = Chrome(DRIVER_PATH)


# if your want to run in headless mode (without GUI)

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

options = Options()
options.headless = True
options.add_argument("--window-size=1920,1200")

driver = webdriver.Chrome(options=options, executable_path=DRIVER_PATH)
driver.get("https://patents.google.com/?q=PFEUJUXMHXWDQS-MHSWQEBQSA-N&patents=false&scholar&oq=PFEUJUXMHXWDQS-MHSWQEBQSA-N")
print(driver.page_source)
print(driver.title)
print(driver.current_url)
#driver.quit()

# how to retrieve an element using find_element

# <html>
#     <head>
#         ... some stuff
#     </head>
#     <body>
#         <h1 class="someclass" id="greatID">Super title</h1>
#     </body>
# </html>

# h1 = driver.find_element_by_name('h1')
# h1 = driver.find_element_by_class_name('someclass')
# h1 = driver.find_element_by_xpath('//h1')
# h1 = driver.find_element_by_id('greatID')

#All these methods also have the find_elements(note the plural) to return a list of elements.

# So here we get the first element
h1 = driver.find_element_by_class_name('style-scope search-result-item')

# and here we get the 10 first results on the page
h1_list = driver.find_elements_by_class_name('style-scope search-result-item')

h1_list

# get first result by xpath

h1 = driver.find_element_by_xpath('//*[@id="htmlContent"]')
h1_link = driver.find_element_by_xpath('//*[@id="link"]')

# we can now acces to the text of an element 
h1.text
h1_link.text

h1_link.get_attribute("href")

<a id="link" href="/scholar/2775812499262969859?q=PFEUJUXMHXWDQS-MHSWQEBQSA-N&amp;patents=false&amp;scholar&amp;oq=PFEUJUXMHXWDQS-MHSWQEBQSA-N" class="style-scope state-modifier">
          <h3 class="style-scope search-result-item"><raw-html class="style-scope search-result-item">
    <span id="htmlContent" class="style-scope raw-html" style="display: inline;">NF-κB inhibitors from Eurycoma longifolia</span>
  </raw-html></h3>
        </a>

for i in h1_list:
    print(i.text)

# or even click on it 

### Not working for now h1_link.click()

print(driver.title)
print(driver.current_url)

# Lets take a screenshot

driver.save_screenshot('screenshot.png')