#!/usr/bin/env python
# coding: utf-8

# loading packages


import pandas as pd
import numpy as np

# loading data
df = pd.read_csv(
    "../curated_tablesampled1000.tsv",
    sep = '\t') 



df[['chemo_inchikey', 'bio_lowertaxon']].sample(30)

df_sub = df[['chemo_inchikey', 'bio_lowertaxon']].sample(30)

chemo_inchikey_list = df_sub['chemo_inchikey'].to_list()
bio_lowertaxon_list = df_sub['bio_lowertaxon'].to_list()

df.info()

## Looking up a specific row

df[df['name'].str.contains('Aspergillipeptide', na=False)]

### and ouputting specif columns values of this same row

# prior we can adjust max col width to display a largher number of character than default

pd.set_option('max_colwidth', 4000)
pd.reset_option('max_colwidth')

df[df['name'].str.contains('Aspergillipeptide A', na=False)].chemo_smiles
df[df['name'].str.contains('Aspergillipeptide A', na=False)].bio_lowertaxon
#or by loc 

df.loc[3,'name']
df.loc[3,'chemo_smiles']
df.loc[3,'chemo_inchikey']
df.loc[3,'bio_lowertaxon']